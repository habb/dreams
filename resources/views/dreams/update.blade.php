@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">تفاصيل الحلم</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="/dreams/edit/{{$dream->id}}" method="post">
            @csrf 
            @method('patch')
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Dream Description</label>
                            <textarea type="text" class="form-control" rows="3" placeholder="Enter ..." disabled>{{$dream->description}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Marital Status</label>
                            <br>
                            <label class="form-control" disabled>{{$dream->marital_status}}</label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Age</label>
                            <br>
                            <label class="form-control" disabled>{{$dream->age}}</label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Gender</label>
                            <br>
                            <label class="form-control" disabled>{{$dream->gender}}</label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Work</label>
                            <br>
                            <label class="form-control" disabled>{{$dream->work}}</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <!-- textarea -->
                        <div class="form-group">
                            <label>Interpitation</label>
                            <textarea id="interpretaion" name="interpretaion" class="form-control @error('interpretaion') is-invalid @enderror" rows="10" placeholder="Enter ...">
                            {{$dream->interpretaion}}
                            </textarea>
                        </div>
                    
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
</div>
@endsection