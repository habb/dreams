@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card-body table-responsive p-0 card">
        <table class="table table-hover text-nowrap">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>User</th>
                    <th style="white-space:pre-wrap; word-wrap:break-word">Title</th>
                    <th style="white-space:pre-wrap; word-wrap:break-word">Description</th>
                    <th style="white-space:pre-wrap; word-wrap:break-word">Interpritation</th>
                    <th>Views</th>
                    <th>Paid</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dreams as $dream)
                <tr>
                    <td>{{$dream->id}}</td>
                    <td>{{$dream->user->username}}</td>
                    <td style="white-space:pre-wrap; word-wrap:break-word">{{$dream->title}}</td>
                    <td style="white-space:pre-wrap; word-wrap:break-word">{{$dream->description}}</td>
                    <td style="white-space:pre-wrap; word-wrap:break-word">{{$dream->interpretaion}}</td>
                    <td>{{$dream->views}}</td>
                    <td>{{$dream->paid}}</td>
                    <td>
                        <!-- Call to action buttons -->
                        <ul class="list-inline m-0">
                            <li class="list-inline-item">
                                <button onclick="location.href='/dreams/{{$dream->id}}';" class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                            </li>
                        </ul>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection