<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;
use Illuminate\Notifications\Notifiable;

class Dream extends Model
{
    use Notifiable;

    protected $fillable = [
        'title',
        'description',
        'marital_status',
        'age',
        'gender',
        'work',
        'user_id',
        'interpretaion'
    ];/*,
                            'views',
                            'paid',
                            'interpretaion'];*/

    public function user() {
        return $this->belongsTo(User::class);
    }
}
