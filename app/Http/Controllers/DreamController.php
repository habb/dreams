<?php

namespace App\Http\Controllers;

use App\Dream;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DreamController extends Controller
{
    //

    public function store(Request $request) {

        $request->validate([
            "title"          => "required",
            "description"    => "required",
            "marital_status"    => "required",
            "age"    => "required",
            "gender"    => "required",
            "work"    => "required",
            "user_id"    => "required",
        ]);
        $request["interpretaion"] = "";


        try {
            $dream = Dream::create($request->all());

        }catch(\Illuminate\Database\QueryException $ex){
            return response()->json([
                'data'=> $ex->getMessage(),
                'status'=>404 ,
                'errorMessage'=>'Success'
            ]);
        }



        return response()->json([
            'data'=> $dream,
            'status'=>200 ,
            'errorMessage'=>'Success'
        ]);

    }

    public function answerDream(Request $request) {
        $dream = DB::table('dreams')->where('id',request('dream_id'))
        ->update(['interpretaion' => request('interpretaion')]);

        $dream = DB::table('dreams')->where('id',request('dream_id'))->get();

        return response()->json([
            'data'=> $dream,
            'status'=>200 ,
            'errorMessage'=>'Success'
        ]);
    }
    
    public function getPaidDreams() {


        $dream = Dream::where('paid',0)->paginate();
        //$dreams = DB::table('dreams')->where('paid',0)->get();

        return response()->json([
            'data'=> $dream,
            'status'=>200 ,
            'errorMessage'=>'Success'
        ]);
    }
    public function viewDream() {

        $value = DB::table("dreams")->select("views")->where("id",request("dream_id"))->first();
        $value = $value->views + 1 ;

        $dream = DB::table("dreams")->where('id',request("dream_id"))->update(["views" => $value]);

        $dream = DB::table("dreams")->where('id',request("dream_id"))->get();

        return response()->json([
            'data'=> $dream,
            'status'=>200 ,
            'errorMessage'=>'Success'
        ]);
    }
    public function makeDreamTrue() {

        $dream = DB::table("dreams")->select('isTrue')->where('id',request("dream_id"))
        ->update(["isTrue" => 1]);
        $dream = DB::table("dreams")->where('id',request("dream_id"))->get();

        return response()->json([
            'data'=> $dream,
            'status'=>200 ,
            'errorMessage'=>'Success'
        ]);

    }

    public function getMyDreams() {
        $dreams = DB::table("dreams")->where('user_id',request('user_id'))->get();

        return response()->json([
            'data'=> $dreams,
            'status'=>200 ,
            'errorMessage'=>'Success'
        ]);
    }
}
