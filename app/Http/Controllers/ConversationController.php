<?php

namespace App\Http\Controllers;

use App\Conversation;
use Illuminate\Http\Request;
use App\Dream;
use Illuminate\Support\Facades\DB;

class ConversationController extends Controller
{
    public function addConversation(Request $request) {


        $request->validate([
            "message" => "required",
            "dream_id" => "required",
            "message_from" => "required",
            "message_to" => "required"


        ]);


        try{
            $conv = Conversation::create($request->all());
        }catch(\Illuminate\Database\QueryException $ex){
        return response()->json([
            'data'=> $ex->getMessage(),
            'status'=>404 ,
            'errorMessage'=>'Success'
        ]);




    }
    return response()->json([
        'data'=> $conv,
        'status'=>200 ,
        'errorMessage'=>'Success'
    ]);
    }

    public function getMyDreamWithMessages(Dream $dream) {

        $messages = DB::table("conversations")->where('id',$dream->id)->orderBy('created_at' , 'asc')->get();
        return response()->json([
            'data'=>$dream ,
            'messages'=>$messages ,
             'status'=>200 ,
             'errorMessage'=>'Success']);
    }
}
