<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Validator;

class PersonController extends Controller
{
    public function show(Person $person)
    {

        return response()->json(['data' => $person, 'status' => 200, 'errorMessage' => 'Success']);
    }


    public function index()
    {
        $users = DB::table('users')->get();

        return response()->json(['data' => $users, 'status' => 200, 'errorMessage' => 'Success'], 200);
    }
    public function indexAll()
    {


        $results =  user::paginate();
        return response()->json([
            'data' => $results,
            'status' => 200,
            'errorMessage' => 'Success'

        ]);
    }

    public function loginToSystem(Request $request)
    {

        //$credentials = $request->only('username', 'password');
        $credentials = $this->credentials($request);
        if (Auth::attempt($credentials)) {
            return response()->json(['data' => Auth::user(), 'status' => 200, 'errorMessage' => 'Success'], 200);
        } else {
            return response()->json(['data' => null, 'status' => -10, 'errorMessage' => 'unauthorized'], 200);
        }

        //dd($request);
        /* $user = DB::table('users')->where('username',request('username'))->where('password', Hash::make(request('password')))
        ->get();

        return response()->json(['data'=>$user , 'status'=>200 , 'errorMessage'=>'Success'] , 200);*/
    }


    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => ['required','unique:users'],
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['data' => null, 'status' => -10, 'errorMessage' => $validator->errors()->first()], 200);
        }
        $input = $request->all();
        $user = User::create([
            'name' => $input['username'],
            'username' => $input['username'],
            'userType' => 2,
            'password' => Hash::make($input['password']),
        ]);
            return response()->json(['data' => $user , 'status' => 200, 'errorMessage' => 'Success'], 200);
    }

    protected function credentials(Request $request)
    {
        return [
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            'userType' => 2
        ];
    }
}
