<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dream;
use Illuminate\Support\Facades\DB;

class DreamFrontController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexFrontEnd()
    {
        //$users = DB::table('dreams')->get();
        $dreams = Dream::paginate();
        return view('dreams.index', [
            'dreams' => $dreams
        ]);
    }

    public function updateFrontEnd($dream_id)
    {
        $dream = Dream::find($dream_id);
        return view('dreams.update', [
            'dream' => $dream
        ]);
    }

    public function answerDream($dream_id)
    {

        $data = request()->validate([
            'interpretaion' => 'required',
        ]);

        DB::table('dreams')
            ->where('id',  $dream_id)
            ->update(['interpretaion' => $data['interpretaion']]);

        return redirect('/dreams');
    }
}
