<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    //

    protected $fillable = [
        'dream_id',
        'message',
        'message_from',
        'message_to'

    ];
}
