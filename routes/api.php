<?php

use App\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get("/person/{person}" , 'PersonController@show');
Route::get("/person" , 'PersonController@index');
Route::post("/login",'PersonController@loginToSystem');
Route::post("/register",'PersonController@signup');
Route::get("/allUsers","PersonController@indexAll");



//dream
Route::post("/createDream","DreamController@store");
Route::post("/answerDream" , "DreamController@answerDream");
Route::get("/getPaidDreams" , "DreamController@getPaidDreams");
Route::post("/viewDream" ,"DreamController@viewDream");
Route::post("/makeDreamTrue","DreamController@makeDreamTrue");
Route::post("/getMyDreams","DreamController@getMyDreams");


//books


//conversation
Route::post("/addConversation","ConversationController@addConversation");
Route::get("/getMyDreamWithMessages/{dream}","ConversationController@getMyDreamWithMessages");
